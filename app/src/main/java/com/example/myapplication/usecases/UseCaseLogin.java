package com.example.myapplication.usecases;

import android.app.Application;

import com.example.myapplication.database.UserRepository;

public class UseCaseLogin {
    private UserRepository userRepository;

    public UseCaseLogin(Application application) {
       userRepository =new UserRepository(application);
    }
    public boolean findByName(String username, String password) {
        if(userRepository.findbyName(username,password)) {
            return true;
        }else {
            return false;
        }
    }
}
