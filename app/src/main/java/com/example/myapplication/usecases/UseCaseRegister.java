package com.example.myapplication.usecases;

import android.app.Application;

import com.example.myapplication.database.UserRepository;
import com.example.myapplication.model.User;

public class UseCaseRegister {
    private UserRepository userRepository;

    public UseCaseRegister(Application application) {
        userRepository =new UserRepository(application);
    }

    public void insertUser(User user) {
        userRepository.insertUser(user);
    }
    public boolean findByUserName(String username) {
        if(userRepository.findByUserName(username)) {
            return true;
        }
        else {
            return false;
        }
    }
}
