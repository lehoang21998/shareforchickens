package com.example.myapplication.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "tab_User")
public class User {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "col_id")
    private int idUser;
    @ColumnInfo(name = "col_name")
    private String userName;
    @ColumnInfo(name = "col_email")
    private String email;
    @ColumnInfo(name = "col_password")
    private String passWord;
    @ColumnInfo(name = "col_phone")
    private String numberPhone;

    public User() {
    }

    public User(String userName, String email, String passWord, String numberPhone) {
        this.userName = userName;
        this.email = email;
        this.passWord = passWord;
        this.numberPhone = numberPhone;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getNumberPhone() {
        return numberPhone;
    }

    public void setNumberPhone(String numberPhone) {
        this.numberPhone = numberPhone;
    }
}
