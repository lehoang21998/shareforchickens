package com.example.myapplication.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.model.User;
import com.example.myapplication.viewmodel.RegisterViewModel;
import com.example.myapplication.viewmodel.ViewModelFactory;

public class RegisterActivity extends AppCompatActivity {
    private RegisterViewModel registerViewModel;
    private EditText etUsername,etEmail,etPassword,etPhone;
    private Button btRegister;
    private TextView txIntenLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        registerViewModel = new ViewModelProvider(this, new ViewModelFactory(getApplication())).get(RegisterViewModel.class);
        init();

        btRegister.setOnClickListener(v->{
            String mUsername = etUsername.getText().toString();
            String mEmail = etEmail.getText().toString();
            String mPassword = etPassword.getText().toString();
            String mNumberPhone = etPhone.getText().toString();
            checkDataUser(mUsername, mEmail, mPassword, mNumberPhone);
        });
        txIntenLogin.setOnClickListener(v->{
            startActivity(new Intent(this,SignInActivity.class));
            finish();
        });

    }

    private void checkDataUser(String mUsername, String mEmail, String mPassword, String mNumberPhone) {
        if(mUsername.isEmpty() || mEmail.isEmpty() || mPassword.isEmpty() || mNumberPhone.isEmpty()){
            Toast.makeText(RegisterActivity.this,"Không bỏ trống thông tin nào",Toast.LENGTH_LONG).show();
        }else {
            if (registerViewModel.checkExistUsername(mUsername)){
                Toast.makeText(RegisterActivity.this,"Tên Tài khoản đã tồn tại",Toast.LENGTH_LONG).show();
            }else{
                registerViewModel.insertUser(new User(mUsername,mEmail,mPassword,mNumberPhone));
                startActivity(new Intent(this, SignInActivity.class));
                finish();
            }
        }
    }

    private void init() {
        etUsername = findViewById(R.id.etusernamere);
        etEmail = findViewById(R.id.etgmailre);
        etPassword = findViewById(R.id.etpasswordre);
        etPhone = findViewById(R.id.etphonere);
        btRegister = findViewById(R.id.btsignoutre);
        txIntenLogin = findViewById(R.id.tvSignIN);
    }
}