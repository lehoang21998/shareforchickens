package com.example.myapplication.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.viewmodel.LoginViewModel;
import com.example.myapplication.viewmodel.ViewModelFactory;

public class SignInActivity extends AppCompatActivity implements LoginViewModel.LoginListener {
    private LoginViewModel loginViewModel;
    private EditText etusername,etpassword;
    private Button btLogin;
    private TextView tvIntenRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);


        loginViewModel = new ViewModelProvider(this, new ViewModelFactory(getApplication())).get(LoginViewModel.class);
        loginViewModel.setLoginListener(this);
        init();

        btLogin.setOnClickListener(view->{
            loginViewModel.login();
        });
        etusername.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                loginViewModel.onChangeUserName(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        etpassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                loginViewModel.onChangePassWord(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        tvIntenRegister.setOnClickListener(view->{
            Intent intent =new Intent(this, RegisterActivity.class);
            startActivity(intent);
            finish();
        });
    }

    private void init() {
        etusername=findViewById(R.id.etusername);
        etpassword=findViewById(R.id.etpassword);
        btLogin=findViewById(R.id.btsignin);
        tvIntenRegister=findViewById(R.id.tvSignUp);
    }

    @Override
    public void error(String message) {
        Toast.makeText(SignInActivity.this,message,Toast.LENGTH_LONG).show();
    }

    @Override
    public void success() {
        Intent intent =new Intent(this,HomeActivity.class);
        startActivity(intent);
    }
}