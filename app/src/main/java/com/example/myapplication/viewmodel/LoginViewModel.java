package com.example.myapplication.viewmodel;

import android.app.Application;

import androidx.lifecycle.ViewModel;

import com.example.myapplication.usecases.UseCaseLogin;

public class LoginViewModel extends ViewModel {
    private UseCaseLogin useCaseSignIn;
    private  String userName,passWord;
    private LoginListener loginListener;

    public void setLoginListener(LoginListener loginListener) {
        this.loginListener = loginListener;
    }

    public void onChangeUserName(String userName) {
        this.userName = userName;
    }

    public void onChangePassWord(String passWord) {
        this.passWord = passWord;
    }

    public LoginViewModel(Application application) {
        useCaseSignIn = new UseCaseLogin(application);
    }

    //CHECK ACCOUNT SIGNIN
    public boolean checkLoginUsername(String username, String password){
        if((useCaseSignIn.findByName(username, password))){
            return true;
        }else
            return false;
    }

    public void login(){
        if(userName.isEmpty() || passWord.isEmpty()){
            loginListener.error("Thong tin trong");
        }else {
            if(useCaseSignIn.findByName(userName,passWord)){
                loginListener.success();
            }else {
                loginListener.error("Dang nhap that bai");
            }
        }
    }
    public interface LoginListener{
        void error(String message);
        void success();
    }
}
