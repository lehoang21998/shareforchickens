package com.example.myapplication.viewmodel;

import android.app.Application;

import androidx.lifecycle.ViewModel;

import com.example.myapplication.model.User;
import com.example.myapplication.usecases.UseCaseRegister;

public class RegisterViewModel extends ViewModel {
    private UseCaseRegister useCaseSignUp;

    public RegisterViewModel(Application application) {
        useCaseSignUp = new UseCaseRegister(application);
    }
    //INSERT A USER
    public void insertUser(User user) {
        useCaseSignUp.insertUser(user);
    }

    //CHECK ACCOUNT REGISTER
    public boolean checkExistUsername(String username) {
        if((useCaseSignUp.findByUserName(username))) {
            return true;
        }else
            return false;
    }
}
