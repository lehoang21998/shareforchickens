package com.example.myapplication.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.myapplication.model.User;

@Dao
public interface IUserDao {
    @Insert
    void insertUser(User users);

    @Query("SELECT * FROM tab_User WHERE col_name = :username AND col_password = :password")
    User findByName(String username, String password);

    @Query("SELECT * FROM tab_User WHERE col_name = :username ")
    User findByUserName(String username);


}
