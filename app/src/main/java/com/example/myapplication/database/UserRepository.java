package com.example.myapplication.database;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.room.Insert;

import com.example.myapplication.model.User;

public class UserRepository {
    private IUserDao iUserDao;

    public UserRepository(Application application) {
        UserDatabase database=UserDatabase.getInstance(application);
        iUserDao = database.userDao();
    }
    public void insertUser(User user){
        iUserDao.insertUser(user);
    }

    public boolean findbyName(String username,String password) {
        if(iUserDao.findByName(username,password)== null)
            return false;
        else return true;
    }
    public boolean findByUserName(String username) {
        if(iUserDao.findByUserName(username)== null)
            return false;
        else return true;
    }
}
